package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for BulkQuery:
{
  "type": "object",
  "properties": {
    "id": {
      "description": "Id of the bulk job",
      "type": "string"
    },
    "status": {
      "description": "Status of the bulk job",
      "type": "string"
    }
  }
}
*/

public class BulkQuery {

	@Size(max=1)
	private String id;

	@Size(max=1)
	private String status;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    id = null;
	    status = null;
	}
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
}
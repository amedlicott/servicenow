package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Object:
{
  "type": "object",
  "properties": {
    "objectField": {
      "type": "string"
    }
  }
}
*/

public class Object {

	@Size(max=1)
	private String objectField;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    objectField = null;
	}
	public String getObjectField() {
		return objectField;
	}
	
	public void setObjectField(String objectField) {
		this.objectField = objectField;
	}
}
package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for BulkStatus:
{
  "type": "object",
  "properties": {
    "batchId": {
      "description": "Contact/Leads loaded",
      "type": "string"
    },
    "message": {
      "type": "string"
    },
    "numOfLeadsProcessed": {
      "description": "Contact/Leads loaded",
      "type": "string"
    },
    "numOfRowsFailed": {
      "description": "Failed count",
      "type": "string"
    },
    "numOfRowsWithWarning": {
      "description": "Contact/Leads that had warnings",
      "type": "string"
    },
    "status": {
      "description": "Status of the bulk job",
      "type": "string"
    }
  }
}
*/

public class BulkStatus {

	@Size(max=1)
	private String batchId;

	@Size(max=1)
	private String message;

	@Size(max=1)
	private String numOfLeadsProcessed;

	@Size(max=1)
	private String numOfRowsFailed;

	@Size(max=1)
	private String numOfRowsWithWarning;

	@Size(max=1)
	private String status;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    batchId = null;
	    message = null;
	    numOfLeadsProcessed = null;
	    numOfRowsFailed = null;
	    numOfRowsWithWarning = null;
	    status = null;
	}
	public String getBatchId() {
		return batchId;
	}
	
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	public String getNumOfLeadsProcessed() {
		return numOfLeadsProcessed;
	}
	
	public void setNumOfLeadsProcessed(String numOfLeadsProcessed) {
		this.numOfLeadsProcessed = numOfLeadsProcessed;
	}
	public String getNumOfRowsFailed() {
		return numOfRowsFailed;
	}
	
	public void setNumOfRowsFailed(String numOfRowsFailed) {
		this.numOfRowsFailed = numOfRowsFailed;
	}
	public String getNumOfRowsWithWarning() {
		return numOfRowsWithWarning;
	}
	
	public void setNumOfRowsWithWarning(String numOfRowsWithWarning) {
		this.numOfRowsWithWarning = numOfRowsWithWarning;
	}
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
}
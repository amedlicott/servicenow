package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for IncidentsAttachments:
{
  "type": "object",
  "properties": {
    "size_bytes": {
      "type": "string"
    },
    "file_name": {
      "type": "string"
    },
    "sys_mod_count": {
      "type": "string"
    },
    "sys_updated_on": {
      "type": "string"
    },
    "sys_tags": {
      "type": "string"
    },
    "table_name": {
      "type": "string"
    },
    "sys_id": {
      "type": "string"
    },
    "sys_updated_by": {
      "type": "string"
    },
    "content_type": {
      "type": "string"
    },
    "sys_created_on": {
      "type": "string"
    },
    "size_compressed": {
      "type": "string"
    },
    "table_sys_id": {
      "type": "string"
    },
    "compressed": {
      "type": "string"
    },
    "sys_created_by": {
      "type": "string"
    }
  }
}
*/

public class IncidentsAttachments {

	@Size(max=1)
	private String size_bytes;

	@Size(max=1)
	private String file_name;

	@Size(max=1)
	private String sys_mod_count;

	@Size(max=1)
	private String sys_updated_on;

	@Size(max=1)
	private String sys_tags;

	@Size(max=1)
	private String table_name;

	@Size(max=1)
	private String sys_id;

	@Size(max=1)
	private String sys_updated_by;

	@Size(max=1)
	private String content_type;

	@Size(max=1)
	private String sys_created_on;

	@Size(max=1)
	private String size_compressed;

	@Size(max=1)
	private String table_sys_id;

	@Size(max=1)
	private String compressed;

	@Size(max=1)
	private String sys_created_by;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    size_bytes = null;
	    file_name = null;
	    sys_mod_count = null;
	    sys_updated_on = null;
	    sys_tags = null;
	    table_name = null;
	    sys_id = null;
	    sys_updated_by = null;
	    content_type = null;
	    sys_created_on = null;
	    size_compressed = null;
	    table_sys_id = null;
	    compressed = null;
	    sys_created_by = null;
	}
	public String getSize_bytes() {
		return size_bytes;
	}
	
	public void setSize_bytes(String size_bytes) {
		this.size_bytes = size_bytes;
	}
	public String getFile_name() {
		return file_name;
	}
	
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getSys_mod_count() {
		return sys_mod_count;
	}
	
	public void setSys_mod_count(String sys_mod_count) {
		this.sys_mod_count = sys_mod_count;
	}
	public String getSys_updated_on() {
		return sys_updated_on;
	}
	
	public void setSys_updated_on(String sys_updated_on) {
		this.sys_updated_on = sys_updated_on;
	}
	public String getSys_tags() {
		return sys_tags;
	}
	
	public void setSys_tags(String sys_tags) {
		this.sys_tags = sys_tags;
	}
	public String getTable_name() {
		return table_name;
	}
	
	public void setTable_name(String table_name) {
		this.table_name = table_name;
	}
	public String getSys_id() {
		return sys_id;
	}
	
	public void setSys_id(String sys_id) {
		this.sys_id = sys_id;
	}
	public String getSys_updated_by() {
		return sys_updated_by;
	}
	
	public void setSys_updated_by(String sys_updated_by) {
		this.sys_updated_by = sys_updated_by;
	}
	public String getContent_type() {
		return content_type;
	}
	
	public void setContent_type(String content_type) {
		this.content_type = content_type;
	}
	public String getSys_created_on() {
		return sys_created_on;
	}
	
	public void setSys_created_on(String sys_created_on) {
		this.sys_created_on = sys_created_on;
	}
	public String getSize_compressed() {
		return size_compressed;
	}
	
	public void setSize_compressed(String size_compressed) {
		this.size_compressed = size_compressed;
	}
	public String getTable_sys_id() {
		return table_sys_id;
	}
	
	public void setTable_sys_id(String table_sys_id) {
		this.table_sys_id = table_sys_id;
	}
	public String getCompressed() {
		return compressed;
	}
	
	public void setCompressed(String compressed) {
		this.compressed = compressed;
	}
	public String getSys_created_by() {
		return sys_created_by;
	}
	
	public void setSys_created_by(String sys_created_by) {
		this.sys_created_by = sys_created_by;
	}
}
package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for IncidentsWorkNotes:
{
  "type": "object",
  "properties": {
    "work_notes": {
      "type": "string"
    }
  }
}
*/

public class IncidentsWorkNotes {

	@Size(max=1)
	private String work_notes;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    work_notes = null;
	}
	public String getWork_notes() {
		return work_notes;
	}
	
	public void setWork_notes(String work_notes) {
		this.work_notes = work_notes;
	}
}
package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for ObjectsMetadata:
{
  "type": "object",
  "properties": {
    "fields": {
      "type": "array",
      "items": {
        "$ref": "objectsMetadataFields"
      }
    }
  }
}
*/

public class ObjectsMetadata {

	@Size(max=1)
	private List<com.digitalml.rest.resources.codegentest.ObjectsMetadataFields> fields;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    fields = new ArrayList<com.digitalml.rest.resources.codegentest.ObjectsMetadataFields>();
	}
	public List<com.digitalml.rest.resources.codegentest.ObjectsMetadataFields> getFields() {
		return fields;
	}
	
	public void setFields(List<com.digitalml.rest.resources.codegentest.ObjectsMetadataFields> fields) {
		this.fields = fields;
	}
}
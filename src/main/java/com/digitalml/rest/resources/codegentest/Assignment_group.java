package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Assignment_group:
{
  "type": "object",
  "properties": {
    "display_value": {
      "type": "string"
    },
    "link": {
      "type": "string"
    }
  }
}
*/

public class Assignment_group {

	@Size(max=1)
	private String display_value;

	@Size(max=1)
	private String link;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    display_value = null;
	    link = null;
	}
	public String getDisplay_value() {
		return display_value;
	}
	
	public void setDisplay_value(String display_value) {
		this.display_value = display_value;
	}
	public String getLink() {
		return link;
	}
	
	public void setLink(String link) {
		this.link = link;
	}
}
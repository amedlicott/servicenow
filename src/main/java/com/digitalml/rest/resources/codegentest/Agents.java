package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Agents:
{
  "type": "object",
  "properties": {
    "calendar_integration": {
      "type": "string"
    },
    "country": {
      "type": "string"
    },
    "user_password": {
      "type": "string"
    },
    "last_login_time": {
      "type": "string"
    },
    "sys_updated_on": {
      "type": "string"
    },
    "source": {
      "type": "string"
    },
    "building": {
      "type": "string"
    },
    "web_service_access_only": {
      "type": "string"
    },
    "notification": {
      "type": "string"
    },
    "sys_updated_by": {
      "type": "string"
    },
    "sys_created_on": {
      "type": "string"
    },
    "sys_domain": {
      "$ref": "sys_domain"
    },
    "agent_status": {
      "type": "string"
    },
    "state": {
      "type": "string"
    },
    "vip": {
      "type": "string"
    },
    "sys_created_by": {
      "type": "string"
    },
    "zip": {
      "type": "string"
    },
    "home_phone": {
      "type": "string"
    },
    "time_format": {
      "type": "string"
    },
    "last_login": {
      "type": "string"
    },
    "default_perspective": {
      "type": "string"
    },
    "active": {
      "type": "string"
    },
    "sys_domain_path": {
      "type": "string"
    },
    "phone": {
      "type": "string"
    },
    "cost_center": {
      "$ref": "cost_center"
    },
    "name": {
      "type": "string"
    },
    "employee_number": {
      "type": "string"
    },
    "password_needs_reset": {
      "type": "string"
    },
    "gender": {
      "type": "string"
    },
    "city": {
      "type": "string"
    },
    "user_name": {
      "type": "string"
    },
    "failed_attempts": {
      "type": "string"
    },
    "roles": {
      "type": "string"
    },
    "title": {
      "type": "string"
    },
    "sys_class_name": {
      "type": "string"
    },
    "sys_id": {
      "type": "string"
    },
    "ldap_server": {
      "type": "string"
    },
    "internal_integration_user": {
      "type": "string"
    },
    "street": {
      "type": "string"
    },
    "mobile_phone": {
      "type": "string"
    },
    "company": {
      "$ref": "company"
    },
    "department": {
      "$ref": "department"
    },
    "first_name": {
      "type": "string"
    },
    "email": {
      "type": "string"
    },
    "introduction": {
      "type": "string"
    },
    "preferred_language": {
      "type": "string"
    },
    "manager": {
      "type": "string"
    },
    "locked_out": {
      "type": "string"
    },
    "sys_mod_count": {
      "type": "string"
    },
    "last_name": {
      "type": "string"
    },
    "photo": {
      "type": "string"
    },
    "sys_tags": {
      "type": "string"
    },
    "middle_name": {
      "type": "string"
    },
    "time_zone": {
      "type": "string"
    },
    "schedule": {
      "type": "string"
    },
    "on_schedule": {
      "type": "string"
    },
    "location": {
      "$ref": "location"
    },
    "date_format": {
      "type": "string"
    }
  }
}
*/

public class Agents {

	@Size(max=1)
	private String calendar_integration;

	@Size(max=1)
	private String country;

	@Size(max=1)
	private String user_password;

	@Size(max=1)
	private String last_login_time;

	@Size(max=1)
	private String sys_updated_on;

	@Size(max=1)
	private String source;

	@Size(max=1)
	private String building;

	@Size(max=1)
	private String web_service_access_only;

	@Size(max=1)
	private String notification;

	@Size(max=1)
	private String sys_updated_by;

	@Size(max=1)
	private String sys_created_on;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Sys_domain sys_domain;

	@Size(max=1)
	private String agent_status;

	@Size(max=1)
	private String state;

	@Size(max=1)
	private String vip;

	@Size(max=1)
	private String sys_created_by;

	@Size(max=1)
	private String zip;

	@Size(max=1)
	private String home_phone;

	@Size(max=1)
	private String time_format;

	@Size(max=1)
	private String last_login;

	@Size(max=1)
	private String default_perspective;

	@Size(max=1)
	private String active;

	@Size(max=1)
	private String sys_domain_path;

	@Size(max=1)
	private String phone;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Cost_center cost_center;

	@Size(max=1)
	private String name;

	@Size(max=1)
	private String employee_number;

	@Size(max=1)
	private String password_needs_reset;

	@Size(max=1)
	private String gender;

	@Size(max=1)
	private String city;

	@Size(max=1)
	private String user_name;

	@Size(max=1)
	private String failed_attempts;

	@Size(max=1)
	private String roles;

	@Size(max=1)
	private String title;

	@Size(max=1)
	private String sys_class_name;

	@Size(max=1)
	private String sys_id;

	@Size(max=1)
	private String ldap_server;

	@Size(max=1)
	private String internal_integration_user;

	@Size(max=1)
	private String street;

	@Size(max=1)
	private String mobile_phone;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Company company;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Department department;

	@Size(max=1)
	private String first_name;

	@Size(max=1)
	private String email;

	@Size(max=1)
	private String introduction;

	@Size(max=1)
	private String preferred_language;

	@Size(max=1)
	private String manager;

	@Size(max=1)
	private String locked_out;

	@Size(max=1)
	private String sys_mod_count;

	@Size(max=1)
	private String last_name;

	@Size(max=1)
	private String photo;

	@Size(max=1)
	private String sys_tags;

	@Size(max=1)
	private String middle_name;

	@Size(max=1)
	private String time_zone;

	@Size(max=1)
	private String schedule;

	@Size(max=1)
	private String on_schedule;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Location location;

	@Size(max=1)
	private String date_format;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    calendar_integration = null;
	    country = null;
	    user_password = null;
	    last_login_time = null;
	    sys_updated_on = null;
	    source = null;
	    building = null;
	    web_service_access_only = null;
	    notification = null;
	    sys_updated_by = null;
	    sys_created_on = null;
	    sys_domain = new com.digitalml.rest.resources.codegentest.Sys.domain();
	    agent_status = null;
	    state = null;
	    vip = null;
	    sys_created_by = null;
	    zip = null;
	    home_phone = null;
	    time_format = null;
	    last_login = null;
	    default_perspective = null;
	    active = null;
	    sys_domain_path = null;
	    phone = null;
	    cost_center = new com.digitalml.rest.resources.codegentest.Cost.center();
	    name = null;
	    employee_number = null;
	    password_needs_reset = null;
	    gender = null;
	    city = null;
	    user_name = null;
	    failed_attempts = null;
	    roles = null;
	    title = null;
	    sys_class_name = null;
	    sys_id = null;
	    ldap_server = null;
	    internal_integration_user = null;
	    street = null;
	    mobile_phone = null;
	    company = new com.digitalml.rest.resources.codegentest.Company();
	    department = new com.digitalml.rest.resources.codegentest.Department();
	    first_name = null;
	    email = null;
	    introduction = null;
	    preferred_language = null;
	    manager = null;
	    locked_out = null;
	    sys_mod_count = null;
	    last_name = null;
	    photo = null;
	    sys_tags = null;
	    middle_name = null;
	    time_zone = null;
	    schedule = null;
	    on_schedule = null;
	    location = new com.digitalml.rest.resources.codegentest.Location();
	    date_format = null;
	}
	public String getCalendar_integration() {
		return calendar_integration;
	}
	
	public void setCalendar_integration(String calendar_integration) {
		this.calendar_integration = calendar_integration;
	}
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	public String getUser_password() {
		return user_password;
	}
	
	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}
	public String getLast_login_time() {
		return last_login_time;
	}
	
	public void setLast_login_time(String last_login_time) {
		this.last_login_time = last_login_time;
	}
	public String getSys_updated_on() {
		return sys_updated_on;
	}
	
	public void setSys_updated_on(String sys_updated_on) {
		this.sys_updated_on = sys_updated_on;
	}
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	public String getBuilding() {
		return building;
	}
	
	public void setBuilding(String building) {
		this.building = building;
	}
	public String getWeb_service_access_only() {
		return web_service_access_only;
	}
	
	public void setWeb_service_access_only(String web_service_access_only) {
		this.web_service_access_only = web_service_access_only;
	}
	public String getNotification() {
		return notification;
	}
	
	public void setNotification(String notification) {
		this.notification = notification;
	}
	public String getSys_updated_by() {
		return sys_updated_by;
	}
	
	public void setSys_updated_by(String sys_updated_by) {
		this.sys_updated_by = sys_updated_by;
	}
	public String getSys_created_on() {
		return sys_created_on;
	}
	
	public void setSys_created_on(String sys_created_on) {
		this.sys_created_on = sys_created_on;
	}
	public com.digitalml.rest.resources.codegentest.Sys_domain getSys_domain() {
		return sys_domain;
	}
	
	public void setSys_domain(com.digitalml.rest.resources.codegentest.Sys_domain sys_domain) {
		this.sys_domain = sys_domain;
	}
	public String getAgent_status() {
		return agent_status;
	}
	
	public void setAgent_status(String agent_status) {
		this.agent_status = agent_status;
	}
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	public String getVip() {
		return vip;
	}
	
	public void setVip(String vip) {
		this.vip = vip;
	}
	public String getSys_created_by() {
		return sys_created_by;
	}
	
	public void setSys_created_by(String sys_created_by) {
		this.sys_created_by = sys_created_by;
	}
	public String getZip() {
		return zip;
	}
	
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getHome_phone() {
		return home_phone;
	}
	
	public void setHome_phone(String home_phone) {
		this.home_phone = home_phone;
	}
	public String getTime_format() {
		return time_format;
	}
	
	public void setTime_format(String time_format) {
		this.time_format = time_format;
	}
	public String getLast_login() {
		return last_login;
	}
	
	public void setLast_login(String last_login) {
		this.last_login = last_login;
	}
	public String getDefault_perspective() {
		return default_perspective;
	}
	
	public void setDefault_perspective(String default_perspective) {
		this.default_perspective = default_perspective;
	}
	public String getActive() {
		return active;
	}
	
	public void setActive(String active) {
		this.active = active;
	}
	public String getSys_domain_path() {
		return sys_domain_path;
	}
	
	public void setSys_domain_path(String sys_domain_path) {
		this.sys_domain_path = sys_domain_path;
	}
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public com.digitalml.rest.resources.codegentest.Cost_center getCost_center() {
		return cost_center;
	}
	
	public void setCost_center(com.digitalml.rest.resources.codegentest.Cost_center cost_center) {
		this.cost_center = cost_center;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getEmployee_number() {
		return employee_number;
	}
	
	public void setEmployee_number(String employee_number) {
		this.employee_number = employee_number;
	}
	public String getPassword_needs_reset() {
		return password_needs_reset;
	}
	
	public void setPassword_needs_reset(String password_needs_reset) {
		this.password_needs_reset = password_needs_reset;
	}
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	public String getUser_name() {
		return user_name;
	}
	
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getFailed_attempts() {
		return failed_attempts;
	}
	
	public void setFailed_attempts(String failed_attempts) {
		this.failed_attempts = failed_attempts;
	}
	public String getRoles() {
		return roles;
	}
	
	public void setRoles(String roles) {
		this.roles = roles;
	}
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSys_class_name() {
		return sys_class_name;
	}
	
	public void setSys_class_name(String sys_class_name) {
		this.sys_class_name = sys_class_name;
	}
	public String getSys_id() {
		return sys_id;
	}
	
	public void setSys_id(String sys_id) {
		this.sys_id = sys_id;
	}
	public String getLdap_server() {
		return ldap_server;
	}
	
	public void setLdap_server(String ldap_server) {
		this.ldap_server = ldap_server;
	}
	public String getInternal_integration_user() {
		return internal_integration_user;
	}
	
	public void setInternal_integration_user(String internal_integration_user) {
		this.internal_integration_user = internal_integration_user;
	}
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	public String getMobile_phone() {
		return mobile_phone;
	}
	
	public void setMobile_phone(String mobile_phone) {
		this.mobile_phone = mobile_phone;
	}
	public com.digitalml.rest.resources.codegentest.Company getCompany() {
		return company;
	}
	
	public void setCompany(com.digitalml.rest.resources.codegentest.Company company) {
		this.company = company;
	}
	public com.digitalml.rest.resources.codegentest.Department getDepartment() {
		return department;
	}
	
	public void setDepartment(com.digitalml.rest.resources.codegentest.Department department) {
		this.department = department;
	}
	public String getFirst_name() {
		return first_name;
	}
	
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIntroduction() {
		return introduction;
	}
	
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public String getPreferred_language() {
		return preferred_language;
	}
	
	public void setPreferred_language(String preferred_language) {
		this.preferred_language = preferred_language;
	}
	public String getManager() {
		return manager;
	}
	
	public void setManager(String manager) {
		this.manager = manager;
	}
	public String getLocked_out() {
		return locked_out;
	}
	
	public void setLocked_out(String locked_out) {
		this.locked_out = locked_out;
	}
	public String getSys_mod_count() {
		return sys_mod_count;
	}
	
	public void setSys_mod_count(String sys_mod_count) {
		this.sys_mod_count = sys_mod_count;
	}
	public String getLast_name() {
		return last_name;
	}
	
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getPhoto() {
		return photo;
	}
	
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getSys_tags() {
		return sys_tags;
	}
	
	public void setSys_tags(String sys_tags) {
		this.sys_tags = sys_tags;
	}
	public String getMiddle_name() {
		return middle_name;
	}
	
	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}
	public String getTime_zone() {
		return time_zone;
	}
	
	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}
	public String getSchedule() {
		return schedule;
	}
	
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	public String getOn_schedule() {
		return on_schedule;
	}
	
	public void setOn_schedule(String on_schedule) {
		this.on_schedule = on_schedule;
	}
	public com.digitalml.rest.resources.codegentest.Location getLocation() {
		return location;
	}
	
	public void setLocation(com.digitalml.rest.resources.codegentest.Location location) {
		this.location = location;
	}
	public String getDate_format() {
		return date_format;
	}
	
	public void setDate_format(String date_format) {
		this.date_format = date_format;
	}
}
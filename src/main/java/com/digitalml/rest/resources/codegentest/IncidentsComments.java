package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for IncidentsComments:
{
  "type": "object",
  "properties": {
    "comments": {
      "type": "string"
    }
  }
}
*/

public class IncidentsComments {

	@Size(max=1)
	private String comments;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    comments = null;
	}
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
}
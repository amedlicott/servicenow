package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Accounts:
{
  "type": "object",
  "properties": {
    "parent": {
      "type": "string"
    },
    "country": {
      "type": "string"
    },
    "notes": {
      "type": "string"
    },
    "city": {
      "type": "string"
    },
    "stock_symbol": {
      "type": "string"
    },
    "latitude": {
      "type": "string"
    },
    "discount": {
      "type": "string"
    },
    "sys_updated_on": {
      "type": "string"
    },
    "manufacturer": {
      "type": "string"
    },
    "apple_icon": {
      "type": "string"
    },
    "sys_id": {
      "type": "string"
    },
    "sys_updated_by": {
      "type": "string"
    },
    "market_cap": {
      "type": "string"
    },
    "num_employees": {
      "type": "string"
    },
    "rank_tier": {
      "type": "string"
    },
    "fiscal_year": {
      "type": "string"
    },
    "street": {
      "type": "string"
    },
    "sys_created_on": {
      "type": "string"
    },
    "vendor": {
      "type": "string"
    },
    "contact": {
      "type": "string"
    },
    "stock_price": {
      "type": "string"
    },
    "theme": {
      "type": "string"
    },
    "lat_long_error": {
      "type": "string"
    },
    "state": {
      "type": "string"
    },
    "banner_image": {
      "type": "string"
    },
    "sys_created_by": {
      "type": "string"
    },
    "longitude": {
      "type": "string"
    },
    "vendor_type": {
      "type": "string"
    },
    "zip": {
      "type": "string"
    },
    "profits": {
      "type": "string"
    },
    "website": {
      "type": "string"
    },
    "revenue_per_year": {
      "type": "string"
    },
    "publicly_traded": {
      "type": "string"
    },
    "sys_mod_count": {
      "type": "string"
    },
    "sys_tags": {
      "type": "string"
    },
    "phone": {
      "type": "string"
    },
    "fax_phone": {
      "type": "string"
    },
    "vendor_manager": {
      "type": "string"
    },
    "name": {
      "type": "string"
    },
    "banner_text": {
      "type": "string"
    },
    "customer": {
      "type": "string"
    },
    "primary": {
      "type": "string"
    }
  }
}
*/

public class Accounts {

	@Size(max=1)
	private String parent;

	@Size(max=1)
	private String country;

	@Size(max=1)
	private String notes;

	@Size(max=1)
	private String city;

	@Size(max=1)
	private String stock_symbol;

	@Size(max=1)
	private String latitude;

	@Size(max=1)
	private String discount;

	@Size(max=1)
	private String sys_updated_on;

	@Size(max=1)
	private String manufacturer;

	@Size(max=1)
	private String apple_icon;

	@Size(max=1)
	private String sys_id;

	@Size(max=1)
	private String sys_updated_by;

	@Size(max=1)
	private String market_cap;

	@Size(max=1)
	private String num_employees;

	@Size(max=1)
	private String rank_tier;

	@Size(max=1)
	private String fiscal_year;

	@Size(max=1)
	private String street;

	@Size(max=1)
	private String sys_created_on;

	@Size(max=1)
	private String vendor;

	@Size(max=1)
	private String contact;

	@Size(max=1)
	private String stock_price;

	@Size(max=1)
	private String theme;

	@Size(max=1)
	private String lat_long_error;

	@Size(max=1)
	private String state;

	@Size(max=1)
	private String banner_image;

	@Size(max=1)
	private String sys_created_by;

	@Size(max=1)
	private String longitude;

	@Size(max=1)
	private String vendor_type;

	@Size(max=1)
	private String zip;

	@Size(max=1)
	private String profits;

	@Size(max=1)
	private String website;

	@Size(max=1)
	private String revenue_per_year;

	@Size(max=1)
	private String publicly_traded;

	@Size(max=1)
	private String sys_mod_count;

	@Size(max=1)
	private String sys_tags;

	@Size(max=1)
	private String phone;

	@Size(max=1)
	private String fax_phone;

	@Size(max=1)
	private String vendor_manager;

	@Size(max=1)
	private String name;

	@Size(max=1)
	private String banner_text;

	@Size(max=1)
	private String customer;

	@Size(max=1)
	private String primary;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    parent = null;
	    country = null;
	    notes = null;
	    city = null;
	    stock_symbol = null;
	    latitude = null;
	    discount = null;
	    sys_updated_on = null;
	    manufacturer = null;
	    apple_icon = null;
	    sys_id = null;
	    sys_updated_by = null;
	    market_cap = null;
	    num_employees = null;
	    rank_tier = null;
	    fiscal_year = null;
	    street = null;
	    sys_created_on = null;
	    vendor = null;
	    contact = null;
	    stock_price = null;
	    theme = null;
	    lat_long_error = null;
	    state = null;
	    banner_image = null;
	    sys_created_by = null;
	    longitude = null;
	    vendor_type = null;
	    zip = null;
	    profits = null;
	    website = null;
	    revenue_per_year = null;
	    publicly_traded = null;
	    sys_mod_count = null;
	    sys_tags = null;
	    phone = null;
	    fax_phone = null;
	    vendor_manager = null;
	    name = null;
	    banner_text = null;
	    customer = null;
	    primary = null;
	}
	public String getParent() {
		return parent;
	}
	
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	public String getNotes() {
		return notes;
	}
	
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	public String getStock_symbol() {
		return stock_symbol;
	}
	
	public void setStock_symbol(String stock_symbol) {
		this.stock_symbol = stock_symbol;
	}
	public String getLatitude() {
		return latitude;
	}
	
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getDiscount() {
		return discount;
	}
	
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getSys_updated_on() {
		return sys_updated_on;
	}
	
	public void setSys_updated_on(String sys_updated_on) {
		this.sys_updated_on = sys_updated_on;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getApple_icon() {
		return apple_icon;
	}
	
	public void setApple_icon(String apple_icon) {
		this.apple_icon = apple_icon;
	}
	public String getSys_id() {
		return sys_id;
	}
	
	public void setSys_id(String sys_id) {
		this.sys_id = sys_id;
	}
	public String getSys_updated_by() {
		return sys_updated_by;
	}
	
	public void setSys_updated_by(String sys_updated_by) {
		this.sys_updated_by = sys_updated_by;
	}
	public String getMarket_cap() {
		return market_cap;
	}
	
	public void setMarket_cap(String market_cap) {
		this.market_cap = market_cap;
	}
	public String getNum_employees() {
		return num_employees;
	}
	
	public void setNum_employees(String num_employees) {
		this.num_employees = num_employees;
	}
	public String getRank_tier() {
		return rank_tier;
	}
	
	public void setRank_tier(String rank_tier) {
		this.rank_tier = rank_tier;
	}
	public String getFiscal_year() {
		return fiscal_year;
	}
	
	public void setFiscal_year(String fiscal_year) {
		this.fiscal_year = fiscal_year;
	}
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	public String getSys_created_on() {
		return sys_created_on;
	}
	
	public void setSys_created_on(String sys_created_on) {
		this.sys_created_on = sys_created_on;
	}
	public String getVendor() {
		return vendor;
	}
	
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getContact() {
		return contact;
	}
	
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getStock_price() {
		return stock_price;
	}
	
	public void setStock_price(String stock_price) {
		this.stock_price = stock_price;
	}
	public String getTheme() {
		return theme;
	}
	
	public void setTheme(String theme) {
		this.theme = theme;
	}
	public String getLat_long_error() {
		return lat_long_error;
	}
	
	public void setLat_long_error(String lat_long_error) {
		this.lat_long_error = lat_long_error;
	}
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	public String getBanner_image() {
		return banner_image;
	}
	
	public void setBanner_image(String banner_image) {
		this.banner_image = banner_image;
	}
	public String getSys_created_by() {
		return sys_created_by;
	}
	
	public void setSys_created_by(String sys_created_by) {
		this.sys_created_by = sys_created_by;
	}
	public String getLongitude() {
		return longitude;
	}
	
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getVendor_type() {
		return vendor_type;
	}
	
	public void setVendor_type(String vendor_type) {
		this.vendor_type = vendor_type;
	}
	public String getZip() {
		return zip;
	}
	
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getProfits() {
		return profits;
	}
	
	public void setProfits(String profits) {
		this.profits = profits;
	}
	public String getWebsite() {
		return website;
	}
	
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getRevenue_per_year() {
		return revenue_per_year;
	}
	
	public void setRevenue_per_year(String revenue_per_year) {
		this.revenue_per_year = revenue_per_year;
	}
	public String getPublicly_traded() {
		return publicly_traded;
	}
	
	public void setPublicly_traded(String publicly_traded) {
		this.publicly_traded = publicly_traded;
	}
	public String getSys_mod_count() {
		return sys_mod_count;
	}
	
	public void setSys_mod_count(String sys_mod_count) {
		this.sys_mod_count = sys_mod_count;
	}
	public String getSys_tags() {
		return sys_tags;
	}
	
	public void setSys_tags(String sys_tags) {
		this.sys_tags = sys_tags;
	}
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFax_phone() {
		return fax_phone;
	}
	
	public void setFax_phone(String fax_phone) {
		this.fax_phone = fax_phone;
	}
	public String getVendor_manager() {
		return vendor_manager;
	}
	
	public void setVendor_manager(String vendor_manager) {
		this.vendor_manager = vendor_manager;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getBanner_text() {
		return banner_text;
	}
	
	public void setBanner_text(String banner_text) {
		this.banner_text = banner_text;
	}
	public String getCustomer() {
		return customer;
	}
	
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getPrimary() {
		return primary;
	}
	
	public void setPrimary(String primary) {
		this.primary = primary;
	}
}
package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for ObjectsMetadataFields:
{
  "type": "object",
  "properties": {
    "mask": {
      "type": "string"
    },
    "type": {
      "type": "string"
    },
    "vendorDisplayName": {
      "type": "string"
    },
    "vendorPath": {
      "type": "string"
    },
    "vendorReadOnly": {
      "type": "boolean"
    },
    "vendorRequired": {
      "type": "boolean"
    }
  }
}
*/

public class ObjectsMetadataFields {

	@Size(max=1)
	private String mask;

	@Size(max=1)
	private String type;

	@Size(max=1)
	private String vendorDisplayName;

	@Size(max=1)
	private String vendorPath;

	@Size(max=1)
	private boolean vendorReadOnly;

	@Size(max=1)
	private boolean vendorRequired;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    mask = null;
	    type = null;
	    vendorDisplayName = null;
	    vendorPath = null;
	    
	    
	}
	public String getMask() {
		return mask;
	}
	
	public void setMask(String mask) {
		this.mask = mask;
	}
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	public String getVendorDisplayName() {
		return vendorDisplayName;
	}
	
	public void setVendorDisplayName(String vendorDisplayName) {
		this.vendorDisplayName = vendorDisplayName;
	}
	public String getVendorPath() {
		return vendorPath;
	}
	
	public void setVendorPath(String vendorPath) {
		this.vendorPath = vendorPath;
	}
	public boolean getVendorReadOnly() {
		return vendorReadOnly;
	}
	
	public void setVendorReadOnly(boolean vendorReadOnly) {
		this.vendorReadOnly = vendorReadOnly;
	}
	public boolean getVendorRequired() {
		return vendorRequired;
	}
	
	public void setVendorRequired(boolean vendorRequired) {
		this.vendorRequired = vendorRequired;
	}
}
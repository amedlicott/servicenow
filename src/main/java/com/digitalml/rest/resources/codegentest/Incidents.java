package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Incidents:
{
  "type": "object",
  "properties": {
    "parent": {
      "type": "string"
    },
    "made_sla": {
      "type": "string"
    },
    "caused_by": {
      "type": "string"
    },
    "watch_list": {
      "type": "string"
    },
    "sys_updated_on": {
      "type": "string"
    },
    "child_incidents": {
      "type": "string"
    },
    "approval_history": {
      "type": "string"
    },
    "skills": {
      "type": "string"
    },
    "number": {
      "type": "string"
    },
    "resolved_by": {
      "$ref": "resolved_by"
    },
    "sys_updated_by": {
      "type": "string"
    },
    "user_input": {
      "type": "string"
    },
    "opened_by": {
      "$ref": "opened_by"
    },
    "sys_created_on": {
      "type": "string"
    },
    "sys_domain": {
      "$ref": "sys_domain"
    },
    "state": {
      "type": "string"
    },
    "sys_created_by": {
      "type": "string"
    },
    "knowledge": {
      "type": "string"
    },
    "order": {
      "type": "string"
    },
    "calendar_stc": {
      "type": "string"
    },
    "closed_at": {
      "type": "string"
    },
    "delivery_plan": {
      "type": "string"
    },
    "cmdb_ci": {
      "$ref": "cmdb_ci"
    },
    "impact": {
      "type": "string"
    },
    "work_notes_list": {
      "type": "string"
    },
    "active": {
      "type": "string"
    },
    "priority": {
      "type": "string"
    },
    "sys_domain_path": {
      "type": "string"
    },
    "rfc": {
      "type": "string"
    },
    "time_worked": {
      "type": "string"
    },
    "expected_start": {
      "type": "string"
    },
    "opened_at": {
      "type": "string"
    },
    "group_list": {
      "type": "string"
    },
    "business_duration": {
      "type": "string"
    },
    "work_end": {
      "type": "string"
    },
    "caller_id": {
      "type": "string"
    },
    "resolved_at": {
      "type": "string"
    },
    "approval_set": {
      "type": "string"
    },
    "work_notes": {
      "type": "string"
    },
    "short_description": {
      "type": "string"
    },
    "delivery_task": {
      "type": "string"
    },
    "work_start": {
      "type": "string"
    },
    "correlation_display": {
      "type": "string"
    },
    "close_code": {
      "type": "string"
    },
    "assignment_group": {
      "$ref": "assignment_group"
    },
    "business_stc": {
      "type": "string"
    },
    "description": {
      "type": "string"
    },
    "calendar_duration": {
      "type": "string"
    },
    "close_notes": {
      "type": "string"
    },
    "notify": {
      "type": "string"
    },
    "sys_class_name": {
      "type": "string"
    },
    "follow_up": {
      "type": "string"
    },
    "closed_by": {
      "$ref": "closed_by"
    },
    "parent_incident": {
      "type": "string"
    },
    "sys_id": {
      "type": "string"
    },
    "incident_state": {
      "type": "string"
    },
    "urgency": {
      "type": "string"
    },
    "problem_id": {
      "$ref": "problem_id"
    },
    "company": {
      "type": "string"
    },
    "reassignment_count": {
      "type": "string"
    },
    "activity_due": {
      "type": "string"
    },
    "assigned_to": {
      "$ref": "assigned_to"
    },
    "severity": {
      "type": "string"
    },
    "comments": {
      "type": "string"
    },
    "sla_due": {
      "type": "string"
    },
    "sys_mod_count": {
      "type": "string"
    },
    "due_date": {
      "type": "string"
    },
    "comments_and_work_notes": {
      "type": "string"
    },
    "u_weid": {
      "type": "string"
    },
    "reopen_count": {
      "type": "string"
    },
    "sys_tags": {
      "type": "string"
    },
    "escalation": {
      "type": "string"
    },
    "correlation_id": {
      "type": "string"
    },
    "location": {
      "$ref": "location"
    },
    "category": {
      "type": "string"
    }
  }
}
*/

public class Incidents {

	@Size(max=1)
	private String parent;

	@Size(max=1)
	private String made_sla;

	@Size(max=1)
	private String caused_by;

	@Size(max=1)
	private String watch_list;

	@Size(max=1)
	private String sys_updated_on;

	@Size(max=1)
	private String child_incidents;

	@Size(max=1)
	private String approval_history;

	@Size(max=1)
	private String skills;

	@Size(max=1)
	private String number;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Resolved_by resolved_by;

	@Size(max=1)
	private String sys_updated_by;

	@Size(max=1)
	private String user_input;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Opened_by opened_by;

	@Size(max=1)
	private String sys_created_on;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Sys_domain sys_domain;

	@Size(max=1)
	private String state;

	@Size(max=1)
	private String sys_created_by;

	@Size(max=1)
	private String knowledge;

	@Size(max=1)
	private String order;

	@Size(max=1)
	private String calendar_stc;

	@Size(max=1)
	private String closed_at;

	@Size(max=1)
	private String delivery_plan;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Cmdb_ci cmdb_ci;

	@Size(max=1)
	private String impact;

	@Size(max=1)
	private String work_notes_list;

	@Size(max=1)
	private String active;

	@Size(max=1)
	private String priority;

	@Size(max=1)
	private String sys_domain_path;

	@Size(max=1)
	private String rfc;

	@Size(max=1)
	private String time_worked;

	@Size(max=1)
	private String expected_start;

	@Size(max=1)
	private String opened_at;

	@Size(max=1)
	private String group_list;

	@Size(max=1)
	private String business_duration;

	@Size(max=1)
	private String work_end;

	@Size(max=1)
	private String caller_id;

	@Size(max=1)
	private String resolved_at;

	@Size(max=1)
	private String approval_set;

	@Size(max=1)
	private String work_notes;

	@Size(max=1)
	private String short_description;

	@Size(max=1)
	private String delivery_task;

	@Size(max=1)
	private String work_start;

	@Size(max=1)
	private String correlation_display;

	@Size(max=1)
	private String close_code;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Assignment_group assignment_group;

	@Size(max=1)
	private String business_stc;

	@Size(max=1)
	private String description;

	@Size(max=1)
	private String calendar_duration;

	@Size(max=1)
	private String close_notes;

	@Size(max=1)
	private String notify;

	@Size(max=1)
	private String sys_class_name;

	@Size(max=1)
	private String follow_up;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Closed_by closed_by;

	@Size(max=1)
	private String parent_incident;

	@Size(max=1)
	private String sys_id;

	@Size(max=1)
	private String incident_state;

	@Size(max=1)
	private String urgency;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Problem_id problem_id;

	@Size(max=1)
	private String company;

	@Size(max=1)
	private String reassignment_count;

	@Size(max=1)
	private String activity_due;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Assigned_to assigned_to;

	@Size(max=1)
	private String severity;

	@Size(max=1)
	private String comments;

	@Size(max=1)
	private String sla_due;

	@Size(max=1)
	private String sys_mod_count;

	@Size(max=1)
	private String due_date;

	@Size(max=1)
	private String comments_and_work_notes;

	@Size(max=1)
	private String u_weid;

	@Size(max=1)
	private String reopen_count;

	@Size(max=1)
	private String sys_tags;

	@Size(max=1)
	private String escalation;

	@Size(max=1)
	private String correlation_id;

	@Size(max=1)
	private com.digitalml.rest.resources.codegentest.Location location;

	@Size(max=1)
	private String category;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    parent = null;
	    made_sla = null;
	    caused_by = null;
	    watch_list = null;
	    sys_updated_on = null;
	    child_incidents = null;
	    approval_history = null;
	    skills = null;
	    number = null;
	    resolved_by = new com.digitalml.rest.resources.codegentest.Resolved.by();
	    sys_updated_by = null;
	    user_input = null;
	    opened_by = new com.digitalml.rest.resources.codegentest.Opened.by();
	    sys_created_on = null;
	    sys_domain = new com.digitalml.rest.resources.codegentest.Sys.domain();
	    state = null;
	    sys_created_by = null;
	    knowledge = null;
	    order = null;
	    calendar_stc = null;
	    closed_at = null;
	    delivery_plan = null;
	    cmdb_ci = new com.digitalml.rest.resources.codegentest.Cmdb.ci();
	    impact = null;
	    work_notes_list = null;
	    active = null;
	    priority = null;
	    sys_domain_path = null;
	    rfc = null;
	    time_worked = null;
	    expected_start = null;
	    opened_at = null;
	    group_list = null;
	    business_duration = null;
	    work_end = null;
	    caller_id = null;
	    resolved_at = null;
	    approval_set = null;
	    work_notes = null;
	    short_description = null;
	    delivery_task = null;
	    work_start = null;
	    correlation_display = null;
	    close_code = null;
	    assignment_group = new com.digitalml.rest.resources.codegentest.Assignment.group();
	    business_stc = null;
	    description = null;
	    calendar_duration = null;
	    close_notes = null;
	    notify = null;
	    sys_class_name = null;
	    follow_up = null;
	    closed_by = new com.digitalml.rest.resources.codegentest.Closed.by();
	    parent_incident = null;
	    sys_id = null;
	    incident_state = null;
	    urgency = null;
	    problem_id = new com.digitalml.rest.resources.codegentest.Problem.id();
	    company = null;
	    reassignment_count = null;
	    activity_due = null;
	    assigned_to = new com.digitalml.rest.resources.codegentest.Assigned.to();
	    severity = null;
	    comments = null;
	    sla_due = null;
	    sys_mod_count = null;
	    due_date = null;
	    comments_and_work_notes = null;
	    u_weid = null;
	    reopen_count = null;
	    sys_tags = null;
	    escalation = null;
	    correlation_id = null;
	    location = new com.digitalml.rest.resources.codegentest.Location();
	    category = null;
	}
	public String getParent() {
		return parent;
	}
	
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getMade_sla() {
		return made_sla;
	}
	
	public void setMade_sla(String made_sla) {
		this.made_sla = made_sla;
	}
	public String getCaused_by() {
		return caused_by;
	}
	
	public void setCaused_by(String caused_by) {
		this.caused_by = caused_by;
	}
	public String getWatch_list() {
		return watch_list;
	}
	
	public void setWatch_list(String watch_list) {
		this.watch_list = watch_list;
	}
	public String getSys_updated_on() {
		return sys_updated_on;
	}
	
	public void setSys_updated_on(String sys_updated_on) {
		this.sys_updated_on = sys_updated_on;
	}
	public String getChild_incidents() {
		return child_incidents;
	}
	
	public void setChild_incidents(String child_incidents) {
		this.child_incidents = child_incidents;
	}
	public String getApproval_history() {
		return approval_history;
	}
	
	public void setApproval_history(String approval_history) {
		this.approval_history = approval_history;
	}
	public String getSkills() {
		return skills;
	}
	
	public void setSkills(String skills) {
		this.skills = skills;
	}
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	public com.digitalml.rest.resources.codegentest.Resolved_by getResolved_by() {
		return resolved_by;
	}
	
	public void setResolved_by(com.digitalml.rest.resources.codegentest.Resolved_by resolved_by) {
		this.resolved_by = resolved_by;
	}
	public String getSys_updated_by() {
		return sys_updated_by;
	}
	
	public void setSys_updated_by(String sys_updated_by) {
		this.sys_updated_by = sys_updated_by;
	}
	public String getUser_input() {
		return user_input;
	}
	
	public void setUser_input(String user_input) {
		this.user_input = user_input;
	}
	public com.digitalml.rest.resources.codegentest.Opened_by getOpened_by() {
		return opened_by;
	}
	
	public void setOpened_by(com.digitalml.rest.resources.codegentest.Opened_by opened_by) {
		this.opened_by = opened_by;
	}
	public String getSys_created_on() {
		return sys_created_on;
	}
	
	public void setSys_created_on(String sys_created_on) {
		this.sys_created_on = sys_created_on;
	}
	public com.digitalml.rest.resources.codegentest.Sys_domain getSys_domain() {
		return sys_domain;
	}
	
	public void setSys_domain(com.digitalml.rest.resources.codegentest.Sys_domain sys_domain) {
		this.sys_domain = sys_domain;
	}
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	public String getSys_created_by() {
		return sys_created_by;
	}
	
	public void setSys_created_by(String sys_created_by) {
		this.sys_created_by = sys_created_by;
	}
	public String getKnowledge() {
		return knowledge;
	}
	
	public void setKnowledge(String knowledge) {
		this.knowledge = knowledge;
	}
	public String getOrder() {
		return order;
	}
	
	public void setOrder(String order) {
		this.order = order;
	}
	public String getCalendar_stc() {
		return calendar_stc;
	}
	
	public void setCalendar_stc(String calendar_stc) {
		this.calendar_stc = calendar_stc;
	}
	public String getClosed_at() {
		return closed_at;
	}
	
	public void setClosed_at(String closed_at) {
		this.closed_at = closed_at;
	}
	public String getDelivery_plan() {
		return delivery_plan;
	}
	
	public void setDelivery_plan(String delivery_plan) {
		this.delivery_plan = delivery_plan;
	}
	public com.digitalml.rest.resources.codegentest.Cmdb_ci getCmdb_ci() {
		return cmdb_ci;
	}
	
	public void setCmdb_ci(com.digitalml.rest.resources.codegentest.Cmdb_ci cmdb_ci) {
		this.cmdb_ci = cmdb_ci;
	}
	public String getImpact() {
		return impact;
	}
	
	public void setImpact(String impact) {
		this.impact = impact;
	}
	public String getWork_notes_list() {
		return work_notes_list;
	}
	
	public void setWork_notes_list(String work_notes_list) {
		this.work_notes_list = work_notes_list;
	}
	public String getActive() {
		return active;
	}
	
	public void setActive(String active) {
		this.active = active;
	}
	public String getPriority() {
		return priority;
	}
	
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getSys_domain_path() {
		return sys_domain_path;
	}
	
	public void setSys_domain_path(String sys_domain_path) {
		this.sys_domain_path = sys_domain_path;
	}
	public String getRfc() {
		return rfc;
	}
	
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getTime_worked() {
		return time_worked;
	}
	
	public void setTime_worked(String time_worked) {
		this.time_worked = time_worked;
	}
	public String getExpected_start() {
		return expected_start;
	}
	
	public void setExpected_start(String expected_start) {
		this.expected_start = expected_start;
	}
	public String getOpened_at() {
		return opened_at;
	}
	
	public void setOpened_at(String opened_at) {
		this.opened_at = opened_at;
	}
	public String getGroup_list() {
		return group_list;
	}
	
	public void setGroup_list(String group_list) {
		this.group_list = group_list;
	}
	public String getBusiness_duration() {
		return business_duration;
	}
	
	public void setBusiness_duration(String business_duration) {
		this.business_duration = business_duration;
	}
	public String getWork_end() {
		return work_end;
	}
	
	public void setWork_end(String work_end) {
		this.work_end = work_end;
	}
	public String getCaller_id() {
		return caller_id;
	}
	
	public void setCaller_id(String caller_id) {
		this.caller_id = caller_id;
	}
	public String getResolved_at() {
		return resolved_at;
	}
	
	public void setResolved_at(String resolved_at) {
		this.resolved_at = resolved_at;
	}
	public String getApproval_set() {
		return approval_set;
	}
	
	public void setApproval_set(String approval_set) {
		this.approval_set = approval_set;
	}
	public String getWork_notes() {
		return work_notes;
	}
	
	public void setWork_notes(String work_notes) {
		this.work_notes = work_notes;
	}
	public String getShort_description() {
		return short_description;
	}
	
	public void setShort_description(String short_description) {
		this.short_description = short_description;
	}
	public String getDelivery_task() {
		return delivery_task;
	}
	
	public void setDelivery_task(String delivery_task) {
		this.delivery_task = delivery_task;
	}
	public String getWork_start() {
		return work_start;
	}
	
	public void setWork_start(String work_start) {
		this.work_start = work_start;
	}
	public String getCorrelation_display() {
		return correlation_display;
	}
	
	public void setCorrelation_display(String correlation_display) {
		this.correlation_display = correlation_display;
	}
	public String getClose_code() {
		return close_code;
	}
	
	public void setClose_code(String close_code) {
		this.close_code = close_code;
	}
	public com.digitalml.rest.resources.codegentest.Assignment_group getAssignment_group() {
		return assignment_group;
	}
	
	public void setAssignment_group(com.digitalml.rest.resources.codegentest.Assignment_group assignment_group) {
		this.assignment_group = assignment_group;
	}
	public String getBusiness_stc() {
		return business_stc;
	}
	
	public void setBusiness_stc(String business_stc) {
		this.business_stc = business_stc;
	}
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCalendar_duration() {
		return calendar_duration;
	}
	
	public void setCalendar_duration(String calendar_duration) {
		this.calendar_duration = calendar_duration;
	}
	public String getClose_notes() {
		return close_notes;
	}
	
	public void setClose_notes(String close_notes) {
		this.close_notes = close_notes;
	}
	public String getNotify() {
		return notify;
	}
	
	public void setNotify(String notify) {
		this.notify = notify;
	}
	public String getSys_class_name() {
		return sys_class_name;
	}
	
	public void setSys_class_name(String sys_class_name) {
		this.sys_class_name = sys_class_name;
	}
	public String getFollow_up() {
		return follow_up;
	}
	
	public void setFollow_up(String follow_up) {
		this.follow_up = follow_up;
	}
	public com.digitalml.rest.resources.codegentest.Closed_by getClosed_by() {
		return closed_by;
	}
	
	public void setClosed_by(com.digitalml.rest.resources.codegentest.Closed_by closed_by) {
		this.closed_by = closed_by;
	}
	public String getParent_incident() {
		return parent_incident;
	}
	
	public void setParent_incident(String parent_incident) {
		this.parent_incident = parent_incident;
	}
	public String getSys_id() {
		return sys_id;
	}
	
	public void setSys_id(String sys_id) {
		this.sys_id = sys_id;
	}
	public String getIncident_state() {
		return incident_state;
	}
	
	public void setIncident_state(String incident_state) {
		this.incident_state = incident_state;
	}
	public String getUrgency() {
		return urgency;
	}
	
	public void setUrgency(String urgency) {
		this.urgency = urgency;
	}
	public com.digitalml.rest.resources.codegentest.Problem_id getProblem_id() {
		return problem_id;
	}
	
	public void setProblem_id(com.digitalml.rest.resources.codegentest.Problem_id problem_id) {
		this.problem_id = problem_id;
	}
	public String getCompany() {
		return company;
	}
	
	public void setCompany(String company) {
		this.company = company;
	}
	public String getReassignment_count() {
		return reassignment_count;
	}
	
	public void setReassignment_count(String reassignment_count) {
		this.reassignment_count = reassignment_count;
	}
	public String getActivity_due() {
		return activity_due;
	}
	
	public void setActivity_due(String activity_due) {
		this.activity_due = activity_due;
	}
	public com.digitalml.rest.resources.codegentest.Assigned_to getAssigned_to() {
		return assigned_to;
	}
	
	public void setAssigned_to(com.digitalml.rest.resources.codegentest.Assigned_to assigned_to) {
		this.assigned_to = assigned_to;
	}
	public String getSeverity() {
		return severity;
	}
	
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getSla_due() {
		return sla_due;
	}
	
	public void setSla_due(String sla_due) {
		this.sla_due = sla_due;
	}
	public String getSys_mod_count() {
		return sys_mod_count;
	}
	
	public void setSys_mod_count(String sys_mod_count) {
		this.sys_mod_count = sys_mod_count;
	}
	public String getDue_date() {
		return due_date;
	}
	
	public void setDue_date(String due_date) {
		this.due_date = due_date;
	}
	public String getComments_and_work_notes() {
		return comments_and_work_notes;
	}
	
	public void setComments_and_work_notes(String comments_and_work_notes) {
		this.comments_and_work_notes = comments_and_work_notes;
	}
	public String getU_weid() {
		return u_weid;
	}
	
	public void setU_weid(String u_weid) {
		this.u_weid = u_weid;
	}
	public String getReopen_count() {
		return reopen_count;
	}
	
	public void setReopen_count(String reopen_count) {
		this.reopen_count = reopen_count;
	}
	public String getSys_tags() {
		return sys_tags;
	}
	
	public void setSys_tags(String sys_tags) {
		this.sys_tags = sys_tags;
	}
	public String getEscalation() {
		return escalation;
	}
	
	public void setEscalation(String escalation) {
		this.escalation = escalation;
	}
	public String getCorrelation_id() {
		return correlation_id;
	}
	
	public void setCorrelation_id(String correlation_id) {
		this.correlation_id = correlation_id;
	}
	public com.digitalml.rest.resources.codegentest.Location getLocation() {
		return location;
	}
	
	public void setLocation(com.digitalml.rest.resources.codegentest.Location location) {
		this.location = location;
	}
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
}
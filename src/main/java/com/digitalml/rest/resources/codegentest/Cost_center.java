package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Cost_center:
{
  "type": "object",
  "properties": {
    "link": {
      "type": "string"
    },
    "value": {
      "type": "string"
    }
  }
}
*/

public class Cost_center {

	@Size(max=1)
	private String link;

	@Size(max=1)
	private String value;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    link = null;
	    value = null;
	}
	public String getLink() {
		return link;
	}
	
	public void setLink(String link) {
		this.link = link;
	}
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}
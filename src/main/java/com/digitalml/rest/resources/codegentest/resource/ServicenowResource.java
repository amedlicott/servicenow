package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.ServicenowService;
	
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetAccountByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetAccountByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetAccountByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateAccountByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateAccountByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateAccountByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteAccountByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteAccountByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteAccountByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByChildObjectIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByChildObjectIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByChildObjectIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.ReplaceByChildObjectIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.ReplaceByChildObjectIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.ReplaceByChildObjectIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateByChildObjectIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateByChildObjectIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateByChildObjectIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteByChildObjectIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteByChildObjectIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteByChildObjectIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsAttachmentsReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsAttachmentsReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsAttachmentsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentAttachmentReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentAttachmentReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentAttachmentInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsWorkNotesReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsWorkNotesReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsWorkNotesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentWorkNoteReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentWorkNoteReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentWorkNoteInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetObjectsMetadataReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetObjectsMetadataReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetObjectsMetadataInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByObjectIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByObjectIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByObjectIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.ReplaceByObjectIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.ReplaceByObjectIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.ReplaceByObjectIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateByObjectIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateByObjectIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateByObjectIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteByObjectIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteByObjectIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteByObjectIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateBulkQueryReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateBulkQueryReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateBulkQueryInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateBulkByObjectNameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateBulkByObjectNameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateBulkByObjectNameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByChildObjectNameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByChildObjectNameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByChildObjectNameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateByChildObjectNameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateByChildObjectNameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateByChildObjectNameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetAgentByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetAgentByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetAgentByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateAgentByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateAgentByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateAgentByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteAgentByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteAgentByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteAgentByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetPingReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetPingReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetPingInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetContactByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetContactByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetContactByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateContactByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateContactByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateContactByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteContactByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteContactByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteContactByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetBulkStatusReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetBulkStatusReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetBulkStatusInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetObjectsReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetObjectsReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetObjectsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetBulkErrorsReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetBulkErrorsReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetBulkErrorsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetBulkByObjectNameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetBulkByObjectNameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetBulkByObjectNameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByObjectNameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByObjectNameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByObjectNameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateByObjectNameReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateByObjectNameReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateByObjectNameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetAgentsReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetAgentsReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetAgentsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateAgentReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateAgentReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateAgentInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsAttachmentByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsAttachmentByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsAttachmentByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteIncidentsAttachmentByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteIncidentsAttachmentByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteIncidentsAttachmentByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateIncidentByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateIncidentByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.UpdateIncidentByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteIncidentByIdReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteIncidentByIdReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteIncidentByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsCommentsReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsCommentsReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsCommentsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentCommentReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentCommentReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentCommentInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetAccountsReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetAccountsReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetAccountsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateAccountReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateAccountReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateAccountInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetContactsReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetContactsReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetContactsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateContactReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateContactReturnDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateContactInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: servicenow
	 * 120
	 *
	 * @author admin
	 * @version api-v2
	 *
	 */
	
	@Path("http://console.cloud-elements.com/elements/api-v2/hubs/helpdesk")
		@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class ServicenowResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(ServicenowResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private ServicenowService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.Servicenow.ServicenowServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private ServicenowService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof ServicenowService)) {
			LOGGER.error(implementationClass + " is not an instance of " + ServicenowService.class.getName());
			return null;
		}

		return (ServicenowService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: getAccountById

	Non-functional requirements:
	*/
	
	@GET
	@Path("/accounts/{id}")
	public javax.ws.rs.core.Response getAccountById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		GetAccountByIdInputParametersDTO inputs = new ServicenowService.GetAccountByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			GetAccountByIdReturnDTO returnValue = delegateService.getAccountById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateAccountById

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/accounts/{id}")
	public javax.ws.rs.core.Response updateAccountById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@PathParam("account")@NotEmpty com.digitalml.rest.resources.codegentest.Accounts account) {

		UpdateAccountByIdInputParametersDTO inputs = new ServicenowService.UpdateAccountByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setAccount(account);
	
		try {
			UpdateAccountByIdReturnDTO returnValue = delegateService.updateAccountById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteAccountById

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/accounts/{id}")
	public javax.ws.rs.core.Response deleteAccountById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		DeleteAccountByIdInputParametersDTO inputs = new ServicenowService.DeleteAccountByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			DeleteAccountByIdReturnDTO returnValue = delegateService.deleteAccountById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getByChildObjectId

	Non-functional requirements:
	*/
	
	@GET
	@Path("/{objectName}/{objectId}/{childObjectName}/{childObjectId}")
	public javax.ws.rs.core.Response getByChildObjectId(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("objectName")@NotEmpty String objectName,
		@PathParam("childObjectName")@NotEmpty String childObjectName,
		@PathParam("objectId")@NotEmpty String objectId,
		@PathParam("childObjectId")@NotEmpty String childObjectId) {

		GetByChildObjectIdInputParametersDTO inputs = new ServicenowService.GetByChildObjectIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setObjectName(objectName);
		inputs.setChildObjectName(childObjectName);
		inputs.setObjectId(objectId);
		inputs.setChildObjectId(childObjectId);
	
		try {
			GetByChildObjectIdReturnDTO returnValue = delegateService.getByChildObjectId(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: replaceByChildObjectId

	Non-functional requirements:
	*/
	
	@PUT
	@Path("/{objectName}/{objectId}/{childObjectName}/{childObjectId}")
	public javax.ws.rs.core.Response replaceByChildObjectId(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("objectName")@NotEmpty String objectName,
		@PathParam("childObjectName")@NotEmpty String childObjectName,
		@PathParam("objectId")@NotEmpty String objectId,
		@PathParam("childObjectId")@NotEmpty String childObjectId,
		 com.digitalml.rest.resources.codegentest.Object Body) {

		ReplaceByChildObjectIdInputParametersDTO inputs = new ServicenowService.ReplaceByChildObjectIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setObjectName(objectName);
		inputs.setChildObjectName(childObjectName);
		inputs.setObjectId(objectId);
		inputs.setChildObjectId(childObjectId);
		inputs.setBody(Body);
	
		try {
			ReplaceByChildObjectIdReturnDTO returnValue = delegateService.replaceByChildObjectId(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateByChildObjectId

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/{objectName}/{objectId}/{childObjectName}/{childObjectId}")
	public javax.ws.rs.core.Response updateByChildObjectId(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("objectName")@NotEmpty String objectName,
		@PathParam("childObjectName")@NotEmpty String childObjectName,
		@PathParam("objectId")@NotEmpty String objectId,
		@PathParam("childObjectId")@NotEmpty String childObjectId,
		 com.digitalml.rest.resources.codegentest.Object Body) {

		UpdateByChildObjectIdInputParametersDTO inputs = new ServicenowService.UpdateByChildObjectIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setObjectName(objectName);
		inputs.setChildObjectName(childObjectName);
		inputs.setObjectId(objectId);
		inputs.setChildObjectId(childObjectId);
		inputs.setBody(Body);
	
		try {
			UpdateByChildObjectIdReturnDTO returnValue = delegateService.updateByChildObjectId(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteByChildObjectId

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/{objectName}/{objectId}/{childObjectName}/{childObjectId}")
	public javax.ws.rs.core.Response deleteByChildObjectId(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("objectName")@NotEmpty String objectName,
		@PathParam("childObjectName")@NotEmpty String childObjectName,
		@PathParam("objectId")@NotEmpty String objectId,
		@PathParam("childObjectId")@NotEmpty String childObjectId) {

		DeleteByChildObjectIdInputParametersDTO inputs = new ServicenowService.DeleteByChildObjectIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setObjectName(objectName);
		inputs.setChildObjectName(childObjectName);
		inputs.setObjectId(objectId);
		inputs.setChildObjectId(childObjectId);
	
		try {
			DeleteByChildObjectIdReturnDTO returnValue = delegateService.deleteByChildObjectId(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getIncidentsAttachments

	Non-functional requirements:
	*/
	
	@GET
	@Path("/incidents/{id}/attachments")
	public javax.ws.rs.core.Response getIncidentsAttachments(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@QueryParam("where") String where,
		@QueryParam("pageSize") String pageSize,
		@QueryParam("page") String page) {

		GetIncidentsAttachmentsInputParametersDTO inputs = new ServicenowService.GetIncidentsAttachmentsInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setWhere(where);
		inputs.setPageSize(pageSize);
		inputs.setPage(page);
	
		try {
			GetIncidentsAttachmentsReturnDTO returnValue = delegateService.getIncidentsAttachments(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createIncidentAttachment

	Non-functional requirements:
	*/
	
	@POST
	@Path("/incidents/{id}/attachments")
	public javax.ws.rs.core.Response createIncidentAttachment(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@QueryParam("file")@NotEmpty String file) {

		CreateIncidentAttachmentInputParametersDTO inputs = new ServicenowService.CreateIncidentAttachmentInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setFile(file);
	
		try {
			CreateIncidentAttachmentReturnDTO returnValue = delegateService.createIncidentAttachment(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getIncidentsWorkNotes

	Non-functional requirements:
	*/
	
	@GET
	@Path("/incidents/{id}/work-notes")
	public javax.ws.rs.core.Response getIncidentsWorkNotes(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@QueryParam("pageSize") String pageSize,
		@QueryParam("page") String page) {

		GetIncidentsWorkNotesInputParametersDTO inputs = new ServicenowService.GetIncidentsWorkNotesInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setPageSize(pageSize);
		inputs.setPage(page);
	
		try {
			GetIncidentsWorkNotesReturnDTO returnValue = delegateService.getIncidentsWorkNotes(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createIncidentWorkNote

	Non-functional requirements:
	*/
	
	@POST
	@Path("/incidents/{id}/work-notes")
	public javax.ws.rs.core.Response createIncidentWorkNote(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@PathParam("work-note")@NotEmpty com.digitalml.rest.resources.codegentest.IncidentsWorkNotes work-note) {

		CreateIncidentWorkNoteInputParametersDTO inputs = new ServicenowService.CreateIncidentWorkNoteInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setWorkNote(work-note);
	
		try {
			CreateIncidentWorkNoteReturnDTO returnValue = delegateService.createIncidentWorkNote(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getObjectsMetadata

	Non-functional requirements:
	*/
	
	@GET
	@Path("/objects/{objectName}/metadata")
	public javax.ws.rs.core.Response getObjectsMetadata(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("objectName")@NotEmpty String objectName) {

		GetObjectsMetadataInputParametersDTO inputs = new ServicenowService.GetObjectsMetadataInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setObjectName(objectName);
	
		try {
			GetObjectsMetadataReturnDTO returnValue = delegateService.getObjectsMetadata(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getByObjectId

	Non-functional requirements:
	*/
	
	@GET
	@Path("/{objectName}/{objectId}")
	public javax.ws.rs.core.Response getByObjectId(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("objectName")@NotEmpty String objectName,
		@PathParam("objectId")@NotEmpty String objectId) {

		GetByObjectIdInputParametersDTO inputs = new ServicenowService.GetByObjectIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setObjectName(objectName);
		inputs.setObjectId(objectId);
	
		try {
			GetByObjectIdReturnDTO returnValue = delegateService.getByObjectId(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: replaceByObjectId

	Non-functional requirements:
	*/
	
	@PUT
	@Path("/{objectName}/{objectId}")
	public javax.ws.rs.core.Response replaceByObjectId(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("objectName")@NotEmpty String objectName,
		@PathParam("objectId")@NotEmpty String objectId,
		 com.digitalml.rest.resources.codegentest.Object Body) {

		ReplaceByObjectIdInputParametersDTO inputs = new ServicenowService.ReplaceByObjectIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setObjectName(objectName);
		inputs.setObjectId(objectId);
		inputs.setBody(Body);
	
		try {
			ReplaceByObjectIdReturnDTO returnValue = delegateService.replaceByObjectId(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateByObjectId

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/{objectName}/{objectId}")
	public javax.ws.rs.core.Response updateByObjectId(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("objectName")@NotEmpty String objectName,
		@PathParam("objectId")@NotEmpty String objectId,
		 com.digitalml.rest.resources.codegentest.Object Body) {

		UpdateByObjectIdInputParametersDTO inputs = new ServicenowService.UpdateByObjectIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setObjectName(objectName);
		inputs.setObjectId(objectId);
		inputs.setBody(Body);
	
		try {
			UpdateByObjectIdReturnDTO returnValue = delegateService.updateByObjectId(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteByObjectId

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/{objectName}/{objectId}")
	public javax.ws.rs.core.Response deleteByObjectId(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("objectName")@NotEmpty String objectName,
		@PathParam("objectId")@NotEmpty String objectId) {

		DeleteByObjectIdInputParametersDTO inputs = new ServicenowService.DeleteByObjectIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setObjectName(objectName);
		inputs.setObjectId(objectId);
	
		try {
			DeleteByObjectIdReturnDTO returnValue = delegateService.deleteByObjectId(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createBulkQuery

	Non-functional requirements:
	*/
	
	@POST
	@Path("/bulk/query")
	public javax.ws.rs.core.Response createBulkQuery(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("Elements-Async-Callback-Url") String Elements-Async-Callback-Url,
		@PathParam("q") String q,
		@QueryParam("lastRunDate") String lastRunDate,
		@QueryParam("from") String from,
		@QueryParam("to") String to,
		@QueryParam("continueFromJobId") String continueFromJobId) {

		CreateBulkQueryInputParametersDTO inputs = new ServicenowService.CreateBulkQueryInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setElementsAsyncCallbackUrl(Elements-Async-Callback-Url);
		inputs.setQ(q);
		inputs.setLastRunDate(lastRunDate);
		inputs.setFrom(from);
		inputs.setTo(to);
		inputs.setContinueFromJobId(continueFromJobId);
	
		try {
			CreateBulkQueryReturnDTO returnValue = delegateService.createBulkQuery(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getIncidents

	Non-functional requirements:
	*/
	
	@GET
	@Path("/incidents")
	public javax.ws.rs.core.Response getIncidents(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("where") String where,
		@QueryParam("pageSize") String pageSize,
		@QueryParam("page") String page) {

		GetIncidentsInputParametersDTO inputs = new ServicenowService.GetIncidentsInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setWhere(where);
		inputs.setPageSize(pageSize);
		inputs.setPage(page);
	
		try {
			GetIncidentsReturnDTO returnValue = delegateService.getIncidents(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createIncident

	Non-functional requirements:
	*/
	
	@POST
	@Path("/incidents")
	public javax.ws.rs.core.Response createIncident(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("incident")@NotEmpty com.digitalml.rest.resources.codegentest.Incidents incident) {

		CreateIncidentInputParametersDTO inputs = new ServicenowService.CreateIncidentInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setIncident(incident);
	
		try {
			CreateIncidentReturnDTO returnValue = delegateService.createIncident(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createBulkByObjectName

	Non-functional requirements:
	*/
	
	@POST
	@Path("/bulk/{objectName}")
	public javax.ws.rs.core.Response createBulkByObjectName(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("Elements-Async-Callback-Url") String Elements-Async-Callback-Url,
		@PathParam("objectName")@NotEmpty String objectName,
		@QueryParam("metaData") String metaData,
		@QueryParam("file") String file) {

		CreateBulkByObjectNameInputParametersDTO inputs = new ServicenowService.CreateBulkByObjectNameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setElementsAsyncCallbackUrl(Elements-Async-Callback-Url);
		inputs.setObjectName(objectName);
		inputs.setMetaData(metaData);
		inputs.setFile(file);
	
		try {
			CreateBulkByObjectNameReturnDTO returnValue = delegateService.createBulkByObjectName(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getByChildObjectName

	Non-functional requirements:
	*/
	
	@GET
	@Path("/{objectName}/{objectId}/{childObjectName}")
	public javax.ws.rs.core.Response getByChildObjectName(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("objectName")@NotEmpty String objectName,
		@PathParam("objectId")@NotEmpty String objectId,
		@PathParam("childObjectName")@NotEmpty String childObjectName,
		@QueryParam("where") String where,
		@QueryParam("page") Integer page,
		@QueryParam("pageSize") Integer pageSize) {

		GetByChildObjectNameInputParametersDTO inputs = new ServicenowService.GetByChildObjectNameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setObjectName(objectName);
		inputs.setObjectId(objectId);
		inputs.setChildObjectName(childObjectName);
		inputs.setWhere(where);
		inputs.setPage(page);
		inputs.setPageSize(pageSize);
	
		try {
			GetByChildObjectNameReturnDTO returnValue = delegateService.getByChildObjectName(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createByChildObjectName

	Non-functional requirements:
	*/
	
	@POST
	@Path("/{objectName}/{objectId}/{childObjectName}")
	public javax.ws.rs.core.Response createByChildObjectName(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("objectName")@NotEmpty String objectName,
		@PathParam("objectId")@NotEmpty String objectId,
		@PathParam("childObjectName")@NotEmpty String childObjectName,
		 com.digitalml.rest.resources.codegentest.Object Body) {

		CreateByChildObjectNameInputParametersDTO inputs = new ServicenowService.CreateByChildObjectNameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setObjectName(objectName);
		inputs.setObjectId(objectId);
		inputs.setChildObjectName(childObjectName);
		inputs.setBody(Body);
	
		try {
			CreateByChildObjectNameReturnDTO returnValue = delegateService.createByChildObjectName(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getAgentById

	Non-functional requirements:
	*/
	
	@GET
	@Path("/agents/{id}")
	public javax.ws.rs.core.Response getAgentById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		GetAgentByIdInputParametersDTO inputs = new ServicenowService.GetAgentByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			GetAgentByIdReturnDTO returnValue = delegateService.getAgentById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateAgentById

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/agents/{id}")
	public javax.ws.rs.core.Response updateAgentById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@PathParam("agent")@NotEmpty com.digitalml.rest.resources.codegentest.Agents agent) {

		UpdateAgentByIdInputParametersDTO inputs = new ServicenowService.UpdateAgentByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setAgent(agent);
	
		try {
			UpdateAgentByIdReturnDTO returnValue = delegateService.updateAgentById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteAgentById

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/agents/{id}")
	public javax.ws.rs.core.Response deleteAgentById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		DeleteAgentByIdInputParametersDTO inputs = new ServicenowService.DeleteAgentByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			DeleteAgentByIdReturnDTO returnValue = delegateService.deleteAgentById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getPing

	Non-functional requirements:
	*/
	
	@GET
	@Path("/ping")
	public javax.ws.rs.core.Response getPing(
		@QueryParam("Authorization")@NotEmpty String Authorization) {

		GetPingInputParametersDTO inputs = new ServicenowService.GetPingInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
	
		try {
			GetPingReturnDTO returnValue = delegateService.getPing(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getContactById

	Non-functional requirements:
	*/
	
	@GET
	@Path("/contacts/{id}")
	public javax.ws.rs.core.Response getContactById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		GetContactByIdInputParametersDTO inputs = new ServicenowService.GetContactByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			GetContactByIdReturnDTO returnValue = delegateService.getContactById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateContactById

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/contacts/{id}")
	public javax.ws.rs.core.Response updateContactById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@PathParam("contact")@NotEmpty com.digitalml.rest.resources.codegentest.Contacts contact) {

		UpdateContactByIdInputParametersDTO inputs = new ServicenowService.UpdateContactByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setContact(contact);
	
		try {
			UpdateContactByIdReturnDTO returnValue = delegateService.updateContactById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteContactById

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/contacts/{id}")
	public javax.ws.rs.core.Response deleteContactById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		DeleteContactByIdInputParametersDTO inputs = new ServicenowService.DeleteContactByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			DeleteContactByIdReturnDTO returnValue = delegateService.deleteContactById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getBulkStatus

	Non-functional requirements:
	*/
	
	@GET
	@Path("/bulk/{id}/status")
	public javax.ws.rs.core.Response getBulkStatus(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		GetBulkStatusInputParametersDTO inputs = new ServicenowService.GetBulkStatusInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			GetBulkStatusReturnDTO returnValue = delegateService.getBulkStatus(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getObjects

	Non-functional requirements:
	*/
	
	@GET
	@Path("/objects")
	public javax.ws.rs.core.Response getObjects(
		@QueryParam("Authorization")@NotEmpty String Authorization) {

		GetObjectsInputParametersDTO inputs = new ServicenowService.GetObjectsInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
	
		try {
			GetObjectsReturnDTO returnValue = delegateService.getObjects(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getBulkErrors

	Non-functional requirements:
	*/
	
	@GET
	@Path("/bulk/{id}/errors")
	public javax.ws.rs.core.Response getBulkErrors(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		GetBulkErrorsInputParametersDTO inputs = new ServicenowService.GetBulkErrorsInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			GetBulkErrorsReturnDTO returnValue = delegateService.getBulkErrors(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getBulkByObjectName

	Non-functional requirements:
	*/
	
	@GET
	@Path("/bulk/{id}/{objectName}")
	public javax.ws.rs.core.Response getBulkByObjectName(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@PathParam("objectName")@NotEmpty String objectName) {

		GetBulkByObjectNameInputParametersDTO inputs = new ServicenowService.GetBulkByObjectNameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setObjectName(objectName);
	
		try {
			GetBulkByObjectNameReturnDTO returnValue = delegateService.getBulkByObjectName(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getByObjectName

	Non-functional requirements:
	*/
	
	@GET
	@Path("/{objectName}")
	public javax.ws.rs.core.Response getByObjectName(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("objectName")@NotEmpty String objectName,
		@QueryParam("where") String where,
		@QueryParam("page") Integer page,
		@QueryParam("pageSize") Integer pageSize) {

		GetByObjectNameInputParametersDTO inputs = new ServicenowService.GetByObjectNameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setObjectName(objectName);
		inputs.setWhere(where);
		inputs.setPage(page);
		inputs.setPageSize(pageSize);
	
		try {
			GetByObjectNameReturnDTO returnValue = delegateService.getByObjectName(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createByObjectName

	Non-functional requirements:
	*/
	
	@POST
	@Path("/{objectName}")
	public javax.ws.rs.core.Response createByObjectName(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("objectName")@NotEmpty String objectName,
		 com.digitalml.rest.resources.codegentest.Object Body) {

		CreateByObjectNameInputParametersDTO inputs = new ServicenowService.CreateByObjectNameInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setObjectName(objectName);
		inputs.setBody(Body);
	
		try {
			CreateByObjectNameReturnDTO returnValue = delegateService.createByObjectName(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getAgents

	Non-functional requirements:
	*/
	
	@GET
	@Path("/agents")
	public javax.ws.rs.core.Response getAgents(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("where") String where,
		@QueryParam("pageSize") String pageSize,
		@QueryParam("page") String page) {

		GetAgentsInputParametersDTO inputs = new ServicenowService.GetAgentsInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setWhere(where);
		inputs.setPageSize(pageSize);
		inputs.setPage(page);
	
		try {
			GetAgentsReturnDTO returnValue = delegateService.getAgents(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createAgent

	Non-functional requirements:
	*/
	
	@POST
	@Path("/agents")
	public javax.ws.rs.core.Response createAgent(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("agent")@NotEmpty com.digitalml.rest.resources.codegentest.Agents agent) {

		CreateAgentInputParametersDTO inputs = new ServicenowService.CreateAgentInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setAgent(agent);
	
		try {
			CreateAgentReturnDTO returnValue = delegateService.createAgent(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getIncidentsAttachmentById

	Non-functional requirements:
	*/
	
	@GET
	@Path("/incidents/{incidentId}/attachments/{id}")
	public javax.ws.rs.core.Response getIncidentsAttachmentById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@PathParam("incidentId")@NotEmpty String incidentId) {

		GetIncidentsAttachmentByIdInputParametersDTO inputs = new ServicenowService.GetIncidentsAttachmentByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setIncidentId(incidentId);
	
		try {
			GetIncidentsAttachmentByIdReturnDTO returnValue = delegateService.getIncidentsAttachmentById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteIncidentsAttachmentById

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/incidents/{incidentId}/attachments/{id}")
	public javax.ws.rs.core.Response deleteIncidentsAttachmentById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@PathParam("incidentId")@NotEmpty String incidentId) {

		DeleteIncidentsAttachmentByIdInputParametersDTO inputs = new ServicenowService.DeleteIncidentsAttachmentByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setIncidentId(incidentId);
	
		try {
			DeleteIncidentsAttachmentByIdReturnDTO returnValue = delegateService.deleteIncidentsAttachmentById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getIncidentById

	Non-functional requirements:
	*/
	
	@GET
	@Path("/incidents/{id}")
	public javax.ws.rs.core.Response getIncidentById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		GetIncidentByIdInputParametersDTO inputs = new ServicenowService.GetIncidentByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			GetIncidentByIdReturnDTO returnValue = delegateService.getIncidentById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: updateIncidentById

	Non-functional requirements:
	*/
	
	@PATCH
	@Path("/incidents/{id}")
	public javax.ws.rs.core.Response updateIncidentById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@PathParam("incident")@NotEmpty com.digitalml.rest.resources.codegentest.Incidents incident) {

		UpdateIncidentByIdInputParametersDTO inputs = new ServicenowService.UpdateIncidentByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setIncident(incident);
	
		try {
			UpdateIncidentByIdReturnDTO returnValue = delegateService.updateIncidentById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: deleteIncidentById

	Non-functional requirements:
	*/
	
	@DELETE
	@Path("/incidents/{id}")
	public javax.ws.rs.core.Response deleteIncidentById(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id) {

		DeleteIncidentByIdInputParametersDTO inputs = new ServicenowService.DeleteIncidentByIdInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
	
		try {
			DeleteIncidentByIdReturnDTO returnValue = delegateService.deleteIncidentById(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getIncidentsComments

	Non-functional requirements:
	*/
	
	@GET
	@Path("/incidents/{id}/comments")
	public javax.ws.rs.core.Response getIncidentsComments(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@QueryParam("pageSize") String pageSize,
		@QueryParam("page") String page) {

		GetIncidentsCommentsInputParametersDTO inputs = new ServicenowService.GetIncidentsCommentsInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setPageSize(pageSize);
		inputs.setPage(page);
	
		try {
			GetIncidentsCommentsReturnDTO returnValue = delegateService.getIncidentsComments(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createIncidentComment

	Non-functional requirements:
	*/
	
	@POST
	@Path("/incidents/{id}/comments")
	public javax.ws.rs.core.Response createIncidentComment(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("id")@NotEmpty String id,
		@PathParam("comment")@NotEmpty com.digitalml.rest.resources.codegentest.IncidentsComments comment) {

		CreateIncidentCommentInputParametersDTO inputs = new ServicenowService.CreateIncidentCommentInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setId(id);
		inputs.setComment(comment);
	
		try {
			CreateIncidentCommentReturnDTO returnValue = delegateService.createIncidentComment(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getAccounts

	Non-functional requirements:
	*/
	
	@GET
	@Path("/accounts")
	public javax.ws.rs.core.Response getAccounts(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("where") String where,
		@QueryParam("pageSize") String pageSize,
		@QueryParam("page") String page) {

		GetAccountsInputParametersDTO inputs = new ServicenowService.GetAccountsInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setWhere(where);
		inputs.setPageSize(pageSize);
		inputs.setPage(page);
	
		try {
			GetAccountsReturnDTO returnValue = delegateService.getAccounts(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createAccount

	Non-functional requirements:
	*/
	
	@POST
	@Path("/accounts")
	public javax.ws.rs.core.Response createAccount(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("account")@NotEmpty com.digitalml.rest.resources.codegentest.Accounts account) {

		CreateAccountInputParametersDTO inputs = new ServicenowService.CreateAccountInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setAccount(account);
	
		try {
			CreateAccountReturnDTO returnValue = delegateService.createAccount(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: getContacts

	Non-functional requirements:
	*/
	
	@GET
	@Path("/contacts")
	public javax.ws.rs.core.Response getContacts(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@QueryParam("where") String where,
		@QueryParam("pageSize") String pageSize,
		@QueryParam("page") String page) {

		GetContactsInputParametersDTO inputs = new ServicenowService.GetContactsInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setWhere(where);
		inputs.setPageSize(pageSize);
		inputs.setPage(page);
	
		try {
			GetContactsReturnDTO returnValue = delegateService.getContacts(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: createContact

	Non-functional requirements:
	*/
	
	@POST
	@Path("/contacts")
	public javax.ws.rs.core.Response createContact(
		@QueryParam("Authorization")@NotEmpty String Authorization,
		@PathParam("contact")@NotEmpty com.digitalml.rest.resources.codegentest.Contacts contact) {

		CreateContactInputParametersDTO inputs = new ServicenowService.CreateContactInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setAuthorization(Authorization);
		inputs.setContact(contact);
	
		try {
			CreateContactReturnDTO returnValue = delegateService.createContact(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(304).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}
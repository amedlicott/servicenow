package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class ServicenowTests {

	@Test
	public void testResourceInitialisation() {
		ServicenowResource resource = new ServicenowResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationGetAccountByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getAccountById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetAccountByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getAccountById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetAccountByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getAccountById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationUpdateAccountByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.updateAccountById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Accounts());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateAccountByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.updateAccountById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Accounts());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateAccountByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.updateAccountById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Accounts());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationDeleteAccountByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteAccountById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteAccountByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.deleteAccountById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteAccountByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.deleteAccountById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetByChildObjectIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getByChildObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetByChildObjectIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getByChildObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetByChildObjectIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getByChildObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationReplaceByChildObjectIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.replaceByChildObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationReplaceByChildObjectIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.replaceByChildObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationReplaceByChildObjectIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.replaceByChildObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationUpdateByChildObjectIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.updateByChildObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateByChildObjectIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.updateByChildObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateByChildObjectIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.updateByChildObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationDeleteByChildObjectIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteByChildObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteByChildObjectIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.deleteByChildObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteByChildObjectIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.deleteByChildObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsAttachmentsNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getIncidentsAttachments(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsAttachmentsNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getIncidentsAttachments(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsAttachmentsAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getIncidentsAttachments(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateIncidentAttachmentNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.createIncidentAttachment(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateIncidentAttachmentNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createIncidentAttachment(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateIncidentAttachmentAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createIncidentAttachment(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsWorkNotesNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getIncidentsWorkNotes(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsWorkNotesNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getIncidentsWorkNotes(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsWorkNotesAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getIncidentsWorkNotes(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateIncidentWorkNoteNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.createIncidentWorkNote(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new IncidentsWorkNotes());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateIncidentWorkNoteNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createIncidentWorkNote(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new IncidentsWorkNotes());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateIncidentWorkNoteAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createIncidentWorkNote(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new IncidentsWorkNotes());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetObjectsMetadataNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getObjectsMetadata(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetObjectsMetadataNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getObjectsMetadata(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetObjectsMetadataAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getObjectsMetadata(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetByObjectIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getByObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetByObjectIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getByObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetByObjectIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getByObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationReplaceByObjectIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.replaceByObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationReplaceByObjectIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.replaceByObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationReplaceByObjectIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.replaceByObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationUpdateByObjectIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.updateByObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateByObjectIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.updateByObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateByObjectIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.updateByObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationDeleteByObjectIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteByObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteByObjectIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.deleteByObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteByObjectIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.deleteByObjectId(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateBulkQueryNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.createBulkQuery(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateBulkQueryNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createBulkQuery(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateBulkQueryAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createBulkQuery(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null, null, null, null);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getIncidents(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getIncidents(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getIncidents(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateIncidentNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.createIncident(org.apache.commons.lang3.StringUtils.EMPTY, new Incidents());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateIncidentNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createIncident(org.apache.commons.lang3.StringUtils.EMPTY, new Incidents());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateIncidentAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createIncident(org.apache.commons.lang3.StringUtils.EMPTY, new Incidents());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateBulkByObjectNameNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.createBulkByObjectName(org.apache.commons.lang3.StringUtils.EMPTY, null, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateBulkByObjectNameNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createBulkByObjectName(org.apache.commons.lang3.StringUtils.EMPTY, null, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateBulkByObjectNameAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createBulkByObjectName(org.apache.commons.lang3.StringUtils.EMPTY, null, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetByChildObjectNameNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getByChildObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, 0, 0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetByChildObjectNameNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getByChildObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, 0, 0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetByChildObjectNameAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getByChildObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, 0, 0);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateByChildObjectNameNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.createByChildObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateByChildObjectNameNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createByChildObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateByChildObjectNameAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createByChildObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetAgentByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getAgentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetAgentByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getAgentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetAgentByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getAgentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationUpdateAgentByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.updateAgentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Agents());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateAgentByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.updateAgentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Agents());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateAgentByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.updateAgentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Agents());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationDeleteAgentByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteAgentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteAgentByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.deleteAgentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteAgentByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.deleteAgentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetPingNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getPing(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetPingNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getPing(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetPingAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getPing(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetContactByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getContactById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetContactByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getContactById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetContactByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getContactById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationUpdateContactByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.updateContactById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Contacts());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateContactByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.updateContactById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Contacts());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateContactByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.updateContactById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Contacts());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationDeleteContactByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteContactById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteContactByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.deleteContactById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteContactByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.deleteContactById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetBulkStatusNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getBulkStatus(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetBulkStatusNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getBulkStatus(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetBulkStatusAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getBulkStatus(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetObjectsNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getObjects(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetObjectsNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getObjects(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetObjectsAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getObjects(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetBulkErrorsNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getBulkErrors(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetBulkErrorsNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getBulkErrors(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetBulkErrorsAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getBulkErrors(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetBulkByObjectNameNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getBulkByObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetBulkByObjectNameNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getBulkByObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetBulkByObjectNameAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getBulkByObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetByObjectNameNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getByObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, 0, 0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetByObjectNameNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getByObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, 0, 0);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetByObjectNameAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getByObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, 0, 0);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateByObjectNameNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.createByObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateByObjectNameNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createByObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateByObjectNameAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createByObjectName(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Object());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetAgentsNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getAgents(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetAgentsNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getAgents(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetAgentsAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getAgents(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateAgentNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.createAgent(org.apache.commons.lang3.StringUtils.EMPTY, new Agents());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateAgentNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createAgent(org.apache.commons.lang3.StringUtils.EMPTY, new Agents());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateAgentAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createAgent(org.apache.commons.lang3.StringUtils.EMPTY, new Agents());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsAttachmentByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getIncidentsAttachmentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsAttachmentByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getIncidentsAttachmentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsAttachmentByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getIncidentsAttachmentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationDeleteIncidentsAttachmentByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteIncidentsAttachmentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteIncidentsAttachmentByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.deleteIncidentsAttachmentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteIncidentsAttachmentByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.deleteIncidentsAttachmentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getIncidentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getIncidentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getIncidentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationUpdateIncidentByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.updateIncidentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Incidents());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateIncidentByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.updateIncidentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Incidents());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationUpdateIncidentByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.updateIncidentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new Incidents());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationDeleteIncidentByIdNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteIncidentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteIncidentByIdNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.deleteIncidentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationDeleteIncidentByIdAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.deleteIncidentById(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsCommentsNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getIncidentsComments(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsCommentsNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getIncidentsComments(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetIncidentsCommentsAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getIncidentsComments(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, null, null);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateIncidentCommentNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.createIncidentComment(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new IncidentsComments());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateIncidentCommentNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createIncidentComment(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new IncidentsComments());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateIncidentCommentAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createIncidentComment(org.apache.commons.lang3.StringUtils.EMPTY, org.apache.commons.lang3.StringUtils.EMPTY, new IncidentsComments());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetAccountsNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getAccounts(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetAccountsNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getAccounts(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetAccountsAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getAccounts(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateAccountNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.createAccount(org.apache.commons.lang3.StringUtils.EMPTY, new Accounts());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateAccountNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createAccount(org.apache.commons.lang3.StringUtils.EMPTY, new Accounts());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateAccountAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createAccount(org.apache.commons.lang3.StringUtils.EMPTY, new Accounts());
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationGetContactsNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.getContacts(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetContactsNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.getContacts(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationGetContactsAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.getContacts(org.apache.commons.lang3.StringUtils.EMPTY, null, null, null);
		Assert.assertEquals(404, response.getStatus());
	}

	@Test
	public void testOperationCreateContactNoSecurity() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(null);

		Response response = resource.createContact(org.apache.commons.lang3.StringUtils.EMPTY, new Contacts());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateContactNotAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(unautheticatedSecurityContext);

		Response response = resource.createContact(org.apache.commons.lang3.StringUtils.EMPTY, new Contacts());
		Assert.assertEquals(304, response.getStatus());
	}

	@Test
	public void testOperationCreateContactAuthorised() {
		ServicenowResource resource = new ServicenowResource();
		resource.setSecurityContext(fullyAutheticatedSecurityContext);

		Response response = resource.createContact(org.apache.commons.lang3.StringUtils.EMPTY, new Contacts());
		Assert.assertEquals(404, response.getStatus());
	}

	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}
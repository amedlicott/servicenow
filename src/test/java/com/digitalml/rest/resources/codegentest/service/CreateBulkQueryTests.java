package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Servicenow.ServicenowServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateBulkQueryInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateBulkQueryReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateBulkQueryTests {

	@Test
	public void testOperationCreateBulkQueryBasicMapping()  {
		ServicenowServiceDefaultImpl serviceDefaultImpl = new ServicenowServiceDefaultImpl();
		CreateBulkQueryInputParametersDTO inputs = new CreateBulkQueryInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setElementsAsyncCallbackUrl(null);
		inputs.setQ(null);
		inputs.setLastRunDate(null);
		inputs.setFrom(null);
		inputs.setTo(null);
		inputs.setContinueFromJobId(null);
		CreateBulkQueryReturnDTO returnValue = serviceDefaultImpl.createBulkQuery(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}
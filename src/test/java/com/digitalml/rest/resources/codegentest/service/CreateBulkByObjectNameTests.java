package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Servicenow.ServicenowServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateBulkByObjectNameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateBulkByObjectNameReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateBulkByObjectNameTests {

	@Test
	public void testOperationCreateBulkByObjectNameBasicMapping()  {
		ServicenowServiceDefaultImpl serviceDefaultImpl = new ServicenowServiceDefaultImpl();
		CreateBulkByObjectNameInputParametersDTO inputs = new CreateBulkByObjectNameInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setElementsAsyncCallbackUrl(null);
		inputs.setObjectName(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setMetaData(null);
		inputs.setFile(null);
		CreateBulkByObjectNameReturnDTO returnValue = serviceDefaultImpl.createBulkByObjectName(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}
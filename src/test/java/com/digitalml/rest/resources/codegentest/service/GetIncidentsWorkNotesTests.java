package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Servicenow.ServicenowServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsWorkNotesInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsWorkNotesReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetIncidentsWorkNotesTests {

	@Test
	public void testOperationGetIncidentsWorkNotesBasicMapping()  {
		ServicenowServiceDefaultImpl serviceDefaultImpl = new ServicenowServiceDefaultImpl();
		GetIncidentsWorkNotesInputParametersDTO inputs = new GetIncidentsWorkNotesInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setPageSize(null);
		inputs.setPage(null);
		GetIncidentsWorkNotesReturnDTO returnValue = serviceDefaultImpl.getIncidentsWorkNotes(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}
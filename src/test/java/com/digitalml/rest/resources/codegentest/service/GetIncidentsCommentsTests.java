package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Servicenow.ServicenowServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsCommentsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsCommentsReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetIncidentsCommentsTests {

	@Test
	public void testOperationGetIncidentsCommentsBasicMapping()  {
		ServicenowServiceDefaultImpl serviceDefaultImpl = new ServicenowServiceDefaultImpl();
		GetIncidentsCommentsInputParametersDTO inputs = new GetIncidentsCommentsInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setPageSize(null);
		inputs.setPage(null);
		GetIncidentsCommentsReturnDTO returnValue = serviceDefaultImpl.getIncidentsComments(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}
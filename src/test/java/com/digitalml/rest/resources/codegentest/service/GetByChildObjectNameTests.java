package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Servicenow.ServicenowServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByChildObjectNameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByChildObjectNameReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetByChildObjectNameTests {

	@Test
	public void testOperationGetByChildObjectNameBasicMapping()  {
		ServicenowServiceDefaultImpl serviceDefaultImpl = new ServicenowServiceDefaultImpl();
		GetByChildObjectNameInputParametersDTO inputs = new GetByChildObjectNameInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setObjectName(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setObjectId(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setChildObjectName(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setWhere(null);
		inputs.setPage(0);
		inputs.setPageSize(0);
		GetByChildObjectNameReturnDTO returnValue = serviceDefaultImpl.getByChildObjectName(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}
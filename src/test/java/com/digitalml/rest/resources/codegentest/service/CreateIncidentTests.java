package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Servicenow.ServicenowServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.CreateIncidentReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateIncidentTests {

	@Test
	public void testOperationCreateIncidentBasicMapping()  {
		ServicenowServiceDefaultImpl serviceDefaultImpl = new ServicenowServiceDefaultImpl();
		CreateIncidentInputParametersDTO inputs = new CreateIncidentInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setIncident(new Incidents());
		CreateIncidentReturnDTO returnValue = serviceDefaultImpl.createIncident(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}
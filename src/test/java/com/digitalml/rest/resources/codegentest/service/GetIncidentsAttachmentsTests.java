package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Servicenow.ServicenowServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsAttachmentsInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsAttachmentsReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetIncidentsAttachmentsTests {

	@Test
	public void testOperationGetIncidentsAttachmentsBasicMapping()  {
		ServicenowServiceDefaultImpl serviceDefaultImpl = new ServicenowServiceDefaultImpl();
		GetIncidentsAttachmentsInputParametersDTO inputs = new GetIncidentsAttachmentsInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setWhere(null);
		inputs.setPageSize(null);
		inputs.setPage(null);
		GetIncidentsAttachmentsReturnDTO returnValue = serviceDefaultImpl.getIncidentsAttachments(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}
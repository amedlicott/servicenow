package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Servicenow.ServicenowServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByChildObjectIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetByChildObjectIdReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetByChildObjectIdTests {

	@Test
	public void testOperationGetByChildObjectIdBasicMapping()  {
		ServicenowServiceDefaultImpl serviceDefaultImpl = new ServicenowServiceDefaultImpl();
		GetByChildObjectIdInputParametersDTO inputs = new GetByChildObjectIdInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setObjectName(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setChildObjectName(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setObjectId(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setChildObjectId(org.apache.commons.lang3.StringUtils.EMPTY);
		GetByChildObjectIdReturnDTO returnValue = serviceDefaultImpl.getByChildObjectId(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}
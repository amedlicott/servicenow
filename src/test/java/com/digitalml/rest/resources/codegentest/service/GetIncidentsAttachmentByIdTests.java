package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Servicenow.ServicenowServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsAttachmentByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetIncidentsAttachmentByIdReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetIncidentsAttachmentByIdTests {

	@Test
	public void testOperationGetIncidentsAttachmentByIdBasicMapping()  {
		ServicenowServiceDefaultImpl serviceDefaultImpl = new ServicenowServiceDefaultImpl();
		GetIncidentsAttachmentByIdInputParametersDTO inputs = new GetIncidentsAttachmentByIdInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setIncidentId(org.apache.commons.lang3.StringUtils.EMPTY);
		GetIncidentsAttachmentByIdReturnDTO returnValue = serviceDefaultImpl.getIncidentsAttachmentById(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponseWrapper200());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}
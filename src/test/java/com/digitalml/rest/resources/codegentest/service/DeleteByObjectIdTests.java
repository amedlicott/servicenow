package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Servicenow.ServicenowServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteByObjectIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteByObjectIdReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class DeleteByObjectIdTests {

	@Test
	public void testOperationDeleteByObjectIdBasicMapping()  {
		ServicenowServiceDefaultImpl serviceDefaultImpl = new ServicenowServiceDefaultImpl();
		DeleteByObjectIdInputParametersDTO inputs = new DeleteByObjectIdInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setObjectName(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setObjectId(org.apache.commons.lang3.StringUtils.EMPTY);
		DeleteByObjectIdReturnDTO returnValue = serviceDefaultImpl.deleteByObjectId(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}
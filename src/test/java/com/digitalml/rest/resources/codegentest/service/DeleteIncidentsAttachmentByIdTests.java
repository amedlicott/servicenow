package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Servicenow.ServicenowServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteIncidentsAttachmentByIdInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.DeleteIncidentsAttachmentByIdReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class DeleteIncidentsAttachmentByIdTests {

	@Test
	public void testOperationDeleteIncidentsAttachmentByIdBasicMapping()  {
		ServicenowServiceDefaultImpl serviceDefaultImpl = new ServicenowServiceDefaultImpl();
		DeleteIncidentsAttachmentByIdInputParametersDTO inputs = new DeleteIncidentsAttachmentByIdInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setIncidentId(org.apache.commons.lang3.StringUtils.EMPTY);
		DeleteIncidentsAttachmentByIdReturnDTO returnValue = serviceDefaultImpl.deleteIncidentsAttachmentById(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}
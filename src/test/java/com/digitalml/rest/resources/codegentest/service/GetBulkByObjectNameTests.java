package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Servicenow.ServicenowServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetBulkByObjectNameInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.ServicenowService.GetBulkByObjectNameReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetBulkByObjectNameTests {

	@Test
	public void testOperationGetBulkByObjectNameBasicMapping()  {
		ServicenowServiceDefaultImpl serviceDefaultImpl = new ServicenowServiceDefaultImpl();
		GetBulkByObjectNameInputParametersDTO inputs = new GetBulkByObjectNameInputParametersDTO();
		inputs.setAuthorization(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		inputs.setObjectName(org.apache.commons.lang3.StringUtils.EMPTY);
		GetBulkByObjectNameReturnDTO returnValue = serviceDefaultImpl.getBulkByObjectName(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}